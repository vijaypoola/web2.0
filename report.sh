#!/usr/bin/env sh
parentDir=${PWD##*/}
parentBranch="$(git branch | grep \* | cut -d ' ' -f2)";
echo "SHOW PARENT AND SUBMODULE BRANCHES";
echo "$parentDir";
echo " \_[$parentBranch]";
git status -s;
echo "=============";
./submod/loopAndDo.sh -c 'echo "$i) $childDir"; echo " \_[$childBranch]"; git status -s;' -r;
echo "-------------";
echo "0) SELECT NO SUBMODULE";
